import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import "components"

MainView {
    id: udsmfilestation
    objectName: 'mainView'
    applicationName: 'udsmfilestation.ubuntouchfr'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)
   
    // persistent app settings:
    property var settings: Settings {

        property string url: ''
        property string login: ''
        property string password: ''
	
    }//settings
        
	property string sid: "" 
     
PageStack {
    anchors.fill: parent
    id: pageStack    
        
    Component.onCompleted: {
        pageStack.push(Qt.resolvedUrl("Login.qml")); 
    }
    Component.onDestruction: {
        console.log("####### delete all file in cache download ###### ");
    }
            
}
}

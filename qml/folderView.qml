import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import "components"

    Page {
        id: folderView
        anchors.fill: parent

        header: PageHeader {
            id: header
            title: pathFolder
                
            leadingActionBar.actions: [
                Action {
                    iconName: "back"
                    text: "back"
                    onTriggered: {
                if(folderView.pathFolder.split('/').length < 3){pageStack.pop()}else{folderView.pathFolder = folderView.pathFolder.split("/").slice(0, -1).join("/")};
                    }
                }
            ]
        }
    
    property string pathFolder:"";



        JSONListModel {
            id: jsonlistfolder
            source: udsmfilestation.settings.url+"/webapi/entry.cgi?api=SYNO.FileStation.List&version=2&method=list&_sid="+udsmfilestation.sid+"&folder_path="+folderView.pathFolder
            query: "$.data.files[*]"
        } 
        
       ListView {
           id: listFolder
            anchors.fill: parent
            anchors.top: header.bottom
            model: jsonlistfolder.model

            delegate: ListItem {
                height: layout.height + (divider.visible ? divider.height : 0)
                ListItemLayout {
                    id: layout
                        
                    Icon {
                        name: model.isdir ? "document-open" : "stock_document"
                        SlotsLayout.position: SlotsLayout.Leading 
                        width: units.gu(2)
                    }
                    title.text: model.name
                    Icon {
                        name: "go-next"
                        SlotsLayout.position: SlotsLayout.Trailing;
                        width: units.gu(2)
                        visible: model.isdir
                    }
                }
                
                onClicked: {
                    if(model.isdir){
                        folderView.pathFolder = model.path
                    }else{
                       
                        pageStack.push(Qt.resolvedUrl("OpenWith.qml"),{url: udsmfilestation.settings.url+"/webapi/entry.cgi?api=SYNO.FileStation.Download&version=2&method=download&_sid="+udsmfilestation.sid+"&mode=download&path="+folderView.pathFolder+"/"+model.name, nameFile: model.name})    

                      
                    }
                }
            }

        }
        
    
        
        
    }

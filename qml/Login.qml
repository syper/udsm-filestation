import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import "components"
        
Page {
    id: loginPage
    anchors.fill: parent
    
Image {
    anchors.fill: parent
    fillMode: Image.PreserveAspectCrop
    source: Qt.resolvedUrl("../assets/backgroundlogin.jpg");
}
    
    Column{
        id: column
        anchors.fill: parent
        anchors.topMargin: units.gu(2)          
        spacing: units.gu(2)

        Item {
            width: parent.width
            height: units.gu(5)
            Label {
                text: i18n.tr('UDSM FileStation')
                anchors.centerIn: parent
                font.pixelSize: units.gu(3)
                color: "#ffffff"
            }
        }

        Item {
            width: parent.width
            height: units.gu(14)

            UbuntuShape {
                radius: "medium"
                source: Image {
                    source: Qt.resolvedUrl("../assets/logo.svg");
                }
                height: units.gu(12); width: height;
                anchors.centerIn: parent;


            } 
        }
            
        Rectangle {
            id: url
            height: units.gu(5)
            anchors {
                left: parent.left
                right: parent.right
                leftMargin: units.gu(2)
                rightMargin: units.gu(2)
            }
            color: "transparent"

            Rectangle {
                id: colorurl
                color: "black"
                radius: units.gu(1)
                opacity: 0.5
                anchors.fill: parent
            }
            Icon {
                id: iconurl
                anchors {
                    left: url.left
                    rightMargin: units.gu(1)
                    leftMargin: units.gu(1)
                }
                anchors.verticalCenter: parent.verticalCenter
                height: parent.height*0.5
                width: height
                name: "stock_website"
                color: "#ffffff"
            }
            TextField {
                id: urlField
                anchors {
                    left: iconurl.right
                    right: parent.right
                }
                height: url.height
                color: "white"
                    background: Rectangle {
                                            height: parent.height
                                            color: "transparent"
                                          }

                placeholderText: i18n.tr("https://myddns.synology.me:5000")
                text: udsmfilestation.settings.url
            }
        }            
            

        Rectangle {
            id: login
            height: units.gu(5)
            anchors {
                left: parent.left
                right: parent.right
                leftMargin: units.gu(2)
                rightMargin: units.gu(2)
            }
            color: "transparent"

            Rectangle {
                id: colorlogin
                color: "black"
                radius: units.gu(1)
                opacity: 0.5
                anchors.fill: parent
            }
            Icon {
                id: iconlogin
                anchors {
                    left: login.left
                    rightMargin: units.gu(1)
                    leftMargin: units.gu(1)
                }
                anchors.verticalCenter: parent.verticalCenter
                height: parent.height*0.5
                width: height
                name: "account"
                color: "#ffffff"
            }
            TextField {
                id: loginField
                anchors {
                    left: iconlogin.right
                    right: parent.right
                }
                height: login.height
                color: "white"
                    background: Rectangle {
                                            height: parent.height
                                            color: "transparent"
                                          }

                placeholderText: i18n.tr("Login")
                text: udsmfilestation.settings.login
            }
        }
        
        
        Rectangle {
            id: password
            height: units.gu(5)
            anchors {
                left: parent.left
                right: parent.right
                leftMargin: units.gu(2)
                rightMargin: units.gu(2)
            }
            color: "transparent"

            Rectangle {
                id: colorpassword
                color: "black"
                radius: units.gu(1)
                opacity: 0.5
                anchors.fill: parent
            }
            Icon {
                id: iconpassword
                anchors {
                    left: password.left
                    rightMargin: units.gu(1)
                    leftMargin: units.gu(1)
                }
                anchors.verticalCenter: parent.verticalCenter
                height: parent.height*0.5
                width: height
                name: "user-admin"
                color: "#ffffff"
            }
            TextField {
                id: passwordField
                anchors {
                    left: iconpassword.right
                    right: parent.right
                }
                echoMode: TextInput.Password
                height: password.height
                color: "white"
                    background: Rectangle {
                                            height: parent.height
                                            color: "transparent"
                                          }

                placeholderText: i18n.tr("Password")
                text: udsmfilestation.settings.password
            }
        }
        
        Text{
            id: errorText
            anchors {
                left: parent.left
                right: parent.right
                leftMargin: units.gu(2)
                rightMargin: units.gu(2)
            }
            text: i18n.tr("We have failed to identify you or the url is not correct")
            color: "red"
            visible: false//(connectOk.count == 0)&&(jsonconnect.source.length != 0)
        }
        
        RoundButton {
            height: units.gu(5)
            anchors {
                left: parent.left
                right: parent.right
                leftMargin: units.gu(2)
                rightMargin: units.gu(2)
            }
            text: i18n.tr("Log in")
            onClicked: {
                    
                    jsonconnect.source = urlField.text+"/webapi/auth.cgi?api=SYNO.API.Auth&version=3&method=login&account="+loginField.text+"&passwd="+passwordField.text+"&session=FileStation&format=cookie"

            }
        }

        
        
        
        
                JSONListModel {
                    id: jsonconnect
                    source: udsmfilestation.settings.url+"/webapi/auth.cgi?api=SYNO.API.Auth&version=3&method=login&account="+udsmfilestation.settings.login+"&passwd="+udsmfilestation.settings.password+"&session=FileStation&format=cookie"
                    query: "$."
                }


               ListView {
                    id: connectOk
                    model: jsonconnect.model
                   
                    delegate: Item {
                            Component.onCompleted: {
                                if(model.success){
                                    //errorText.visible = false
                                    udsmfilestation.sid = model.data.sid
                                    udsmfilestation.settings.url = urlField.text
                                    udsmfilestation.settings.login = loginField.text
                                    udsmfilestation.settings.password = passwordField.text
                                    pageStack.push(Qt.resolvedUrl("SharesView.qml"))
                                }else{
                                    //errorText.visible = true
                                }
                            }                
                    }

                }        
        
        
    }            

        
}

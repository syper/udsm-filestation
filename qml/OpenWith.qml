import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Content 1.3
import Ubuntu.DownloadManager 1.2
import FileManager 1.0
import "contenttyperesolver.js" as Resolver

Page {
	id: shareView
        
        header: PageHeader {
            id: header
	        title: i18n.tr("Open with")
                
            leadingActionBar.actions: [
                Action {
                    iconName: "back"
                    text: "back"
                    onTriggered: {
                        pageStack.pop();
                        if(shareView.filefordownload.length > 1){FileManager.deleteFile(shareView.filefordownload)};
                    }
                }
            ]
        }
    
	property var url
	property var nameFile
	property var filefordownload

    Component.onCompleted: {
        var contentType = Resolver.resolveContentType(nameFile)
        cpp.contentType = contentType
        single.download(shareView.url);
    }
        
        
        Component.onDestruction: {
            if(shareView.filefordownload.length > 1){FileManager.deleteFile(shareView.filefordownload)};
        }
            
            
        
    Rectangle {
        id: dl
       anchors.fill: parent
           
           Text{
                id: waitPlease
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                text: i18n.tr("Downloading in progress...")
           }

       ProgressBar {
            minimumValue: 0
            maximumValue: 100
            value: single.progress
            width: units.gu(30)
            height: units.gu(1)
                
            anchors.top: waitPlease.bottom
            anchors.topMargin: units.gu(2)
            anchors.horizontalCenter: parent.horizontalCenter

           SingleDownload {
            id: single
                
            autoStart: true    
                
            onFinished: {
                
             var basePath = path.split("/").slice(0, -1).join("/");
             var newPath = FileManager.saveDownload(nameFile, basePath, path);   
                
                shareView.filefordownload = newPath
                    
                cpp.visible = true
            }
           }
       }
   }
    
	ContentPeerPicker {
		id: cpp
		objectName: "sharePicker"
		anchors.fill: parent
        visible: false        
		contentType: ContentType.Pictures
		handler: ContentHandler.Destination
		showTitle: false
            
		onPeerSelected: {
			var activeTransfer = peer.request()
			if (activeTransfer.state === ContentTransfer.InProgress) {
				activeTransfer.items = [ resultComponent.createObject(parent, {"url": shareView.filefordownload}) ]
				activeTransfer.state = ContentTransfer.Charged
			}
            FileManager.deleteFile(shareView.filefordownload);
            pageStack.pop()
		}

        onCancelPressed: {
                FileManager.deleteFile(shareView.filefordownload);
                pageStack.pop()
        } 
	}

	Component {
		id: resultComponent
		ContentItem { }
	}
}

import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import "components"
    
    
    Page {
        id: main
        anchors.fill: parent

        header: PageHeader {
            id: header
            title: i18n.tr('UDSM FileStation')
                
            leadingActionBar.actions: [

            ]
        }

        Rectangle {
                id: tooltipError
                color: "grey"
                opacity: 0.8
                height: textTooltip.height*1.5
                width: textTooltip.width+units.gu(2)
                radius: height/2
                anchors.horizontalCenter: parent.horizontalCenter
                Text{
                    id: textTooltip
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter                        
                    text: ""
                    color: "white"
                }
                visible: false
        }
        Timer {
            id: falseTooltip
            interval: 3000 
            running: false
            onTriggered: tooltipError.visible = false
        }

        
        JSONListModel {
            id: jsonconnect
            source: udsmfilestation.settings.url+"/webapi/auth.cgi?api=SYNO.API.Auth&version=3&method=login&account="+udsmfilestation.settings.login+"&passwd="+udsmfilestation.settings.password+"&session=FileStation&format=cookie"
            query: "$."
        }
    
    
       ListView {
            id: connectOk
            model: jsonconnect.model
            delegate: Item {
                    Component.onCompleted: {
                        if(model.success){
                            udsmfilestation.sid = model.data.sid
                            tooltipError.visible = true
                            textTooltip.text = i18n.tr("Vous êtes connecté avec succès ")
                            falseTooltip.running = true  
                            jsonlistfolder.source = udsmfilestation.settings.url+"/webapi/entry.cgi?api=SYNO.FileStation.List&version=2&method=list_share&_sid="+udsmfilestation.sid
                        }else{
                            tooltipError.visible = true
                            textTooltip.text = i18n.tr("Erreur de connexion !")
                            falseTooltip.running = true
                            pageStack.pop()
                        }
                    }                
            }

        }
    
        
            
        JSONListModel {
            id: jsonlistfolder
            query: "$.data.shares[*]"
        } 
        
       ListView {
            anchors.fill: parent
            model: jsonlistfolder.model

            delegate: ListItem {
                height: layout.height + (divider.visible ? divider.height : 0)
                ListItemLayout {
                    id: layout
                        
                    Icon {
                        name: model.isdir ? "document-open" : "stock_document"
                        SlotsLayout.position: SlotsLayout.Leading 
                        width: units.gu(2)
                    }
                    title.text: model.name
                    Icon {
                        name: "go-next"
                        SlotsLayout.position: SlotsLayout.Trailing;
                        width: units.gu(2)
                        visible: model.isdir
                    }
                }
                
                onClicked: {
                        pageStack.push(Qt.resolvedUrl("folderView.qml"), {"pathFolder": model.path}); 
                }
            }

        }
                

            
        
        
    }


